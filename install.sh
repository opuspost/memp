#/bin/bash

# Install command line tools
echo "Install command line tools"
xcode-select --install

# Install HomeBrew
echo "Install HomeBrew"
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

# check homebrew
echo "Check homebrew"
brew doctor

# Update repos
echo "Update repositories"
brew update
brew upgrade

##################
# Install PHP-FPM 5.6, 7.0 & 7.1
echo "Install PHP-FPM 5.6, 7.0 & 7.1"
#brew install php@5.6
#brew install php@7.0
brew install php@7.1

#ln -s /usr/local/Cellar/php56/*/sbin/php56-fpm /usr/local/bin/
#ln -s /usr/local/Cellar/php70/*/sbin/php70-fpm /usr/local/bin/
ln -s /usr/local/Cellar/php71/*/sbin/php71-fpm /usr/local/bin/

#ln -s /usr/local/Cellar/php\@5.6/*/bin/php /usr/local/bin/php56
#ln -s /usr/local/Cellar/php\@7.0/*/bin/php /usr/local/bin/php70
ln -s /usr/local/Cellar/php\@7.1/*/bin/php /usr/local/bin/php71

#sed -ie 's/9000/9070/' /usr/local/etc/php/7.0/php-fpm.d/www.conf
sed -ie 's/9000/9071/' /usr/local/etc/php/7.1/php-fpm.d/www.conf

#echo 'export PATH="/usr/local/opt/php@7.1/bin:$PATH"' >> ~/.bash_profile

#TODO
#ln -s /usr/local/Cellar/php\@5.6/5.6.36/bin/php /usr/local/bin/php56

#################
# Install MySQL
echo "Install MySQL 5.7"
brew install mysql@5.7

# Add MySQL to autorun and start
brew services start mysql@5.7

# Run mysql_secure_installation
echo "Run mysql_secure_installation"
mysql_secure_installation

################
# Install NGINX
echo "Install NGINX"
brew install nginx

echo "Create nginx config directories"
mkdir -p /usr/local/etc/nginx/sites-available
mkdir -p /usr/local/etc/nginx/sites-enabled
mkdir -p /usr/local/etc/nginx/conf.d

echo "Change nginx configs"
rm /usr/local/etc/nginx/nginx.conf
cp `dirname $0`/templates/nginx.conf /usr/local/etc/nginx/nginx.conf
cp `dirname $0`/templates/vhost.conf /usr/local/etc/nginx/sites-available/vhost.conf
cp `dirname $0`/templates/upstreams.conf /usr/local/etc/nginx/conf.d/upstreams.conf

# Add symlink to /etc/
echo "Add symlink to /etc/nginx"
sudo ln -s /usr/local/etc/nginx /etc/nginx

echo "Create /var/www and /var/log/nginx directories"
sudo mkdir -p /var/log/nginx
sudo mkdir -p /var/www

sudo chown :staff /var/www
sudo chmod 775 /var/www

# Add bash aliases

echo "Add bash aliases for follow coms:"
echo "nginx.start"
echo "nginx.stop"
echo "nginx.restart"
echo ""
echo "php56-fpm.start"
echo "php56-fpm.stop"
echo "php56-fpm.restart"
echo ""
echo "php70-fpm.start"
echo "php70-fpm.stop"
echo "php70-fpm.restart"
echo ""
echo "php71-fpm.start"
echo "php71-fpm.stop"
echo "php71-fpm.restart"
echo ""
echo "mysql.start"
echo "mysql.stop"
echo "mysql.restart"

cat `dirname $0`/templates/bash_aliases >> ~/.bash_aliases
echo "source ~/.bash_aliases" >> ~/.bash_profile
source ~/.bash_profile

# Add to autorun and restart php56, php70, php71 and nginx
echo "Add to autorun and restart php's and nginx"
#brew services restart php56
#brew services restart php70
brew services restart php71
sudo brew services restart nginx

#######INSTALL NODEJS#########

brew install wget

wget -q ftp://192.168.0.11/soft/4mac/node*.pkg
sudo installer -pkg node*.pkg -target /
rm node*.pkg

#######INSTALL COMPOSER#######

php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('SHA384', 'composer-setup.php') === '544e09ee996cdf60ece3804abc52599c22b1f40f4323403c44d44fdfdd586475ca9813a858088ffbc1f233e9b180f061') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php --install-dir=/usr/local/bin --filename=composer
php -r "unlink('composer-setup.php');"

#######INSTALL JAVA###########

wget -q ftp://192.168.0.11/soft/4mac/jdk*.dmg
DMG=`ls jdk*.dmg`
hdiutil attach ${DMG} -quiet
PKG=`sudo find /Volumes/JDK* -name "*.pkg"`
sudo installer -pkg "$PKG" -target /
hdiutil unmount /Volumes/JDK* -quiet
rm ${DMG}

########INSTALL ELASTICSEARCH2#########

ES2_URL=ftp://192.168.0.11/soft/4mac/elasticsearch-2*
ES2_ARCHIVE=elasticsearch-2*.tar.gz
ES2_DIR=/usr/local/opt/elasticsearch2
ES2_CONFIG=${ES2_DIR}/config/elasticsearch.yml
ES2_PLIST=es2.plist

wget -q ${ES2_URL}
mkdir ${ES2_DIR} && tar -C ${ES2_DIR} -xf ${ES2_ARCHIVE} --strip 1
sed -i '' -e 's/\(.*\)cluster.name\(.*\)/ cluster.name\: el2/' ${ES2_CONFIG}
cp templates/${ES2_PLIST} ~/Library/LaunchAgents/org.${ES2_PLIST}
launchctl load ~/Library/LaunchAgents/org.${ES2_PLIST}
rm ${ES2_ARCHIVE}

########INSTALL ELASTICSEARCH5#########

ES5_URL=ftp://192.168.0.11/soft/4mac/elasticsearch-5*
ES5_ARCHIVE=elasticsearch-5*.tar.gz
ES5_DIR=/usr/local/opt/elasticsearch5
ES5_CONFIG=${ES5_DIR}/config/elasticsearch.yml
ES5_PLIST=es5.plist

wget -q ${ES5_URL}
mkdir ${ES5_DIR} && tar -C ${ES5_DIR} -xf ${ES5_ARCHIVE} --strip 1
sed -i '' -e 's/#http.port\(.*\).$/http.port\11/' ${ES5_CONFIG}
sed -i '' -e 's/^#cluster.name\(.*\)/cluster.name\: el5/' ${ES5_CONFIG}
cp templates/${ES5_PLIST} ~/Library/LaunchAgents/org.${ES5_PLIST}
launchctl load ~/Library/LaunchAgents/org.${ES5_PLIST}
rm ${ES5_ARCHIVE}

source ~/.bash_profile
